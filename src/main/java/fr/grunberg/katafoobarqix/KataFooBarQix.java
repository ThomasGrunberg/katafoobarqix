package fr.grunberg.katafoobarqix;

public class KataFooBarQix {
	static String compute(String number) {
		int numberAsInteger = Integer.parseInt(number);
		String computedString = "";
		if(numberAsInteger % 3 == 0)
			computedString += "Foo";
		if(numberAsInteger % 5 == 0)
			computedString += "Bar";
		if(numberAsInteger % 7 == 0)
			computedString += "Qix";
		String[] digits = number.split("");
		for(String digit : digits) {
			if("0".equals(digit))
				computedString += "*";
			else if(digit.equals("3"))
				computedString += "Foo";
			else if(digit.equals("5"))
				computedString += "Bar";
			else if(digit.equals("7"))
				computedString += "Qix";
		}
		if("".equals(computedString) || computedString.matches("[\\*]*"))
			return number.replace("0", "*");
		else
			return computedString;
	}
}
