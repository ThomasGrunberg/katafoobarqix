package fr.grunberg.katafoobarqix;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class KataFooBarQixTest {
	@Test
	public void computeStep1() {
		assertEquals("1", KataFooBarQix.compute("1"));
		assertEquals("2", KataFooBarQix.compute("2"));
		assertEquals("FooFoo", KataFooBarQix.compute("3"));
		assertEquals("4", KataFooBarQix.compute("4"));
		assertEquals("BarBar", KataFooBarQix.compute("5"));
		assertEquals("Foo", KataFooBarQix.compute("6"));
		assertEquals("QixQix", KataFooBarQix.compute("7"));
		assertEquals("8", KataFooBarQix.compute("8"));
		assertEquals("Foo", KataFooBarQix.compute("9"));
		assertEquals("Foo", KataFooBarQix.compute("13"));
		assertEquals("FooBarBar", KataFooBarQix.compute("15"));
		assertEquals("FooQix", KataFooBarQix.compute("21"));
		assertEquals("FooFooFoo", KataFooBarQix.compute("33"));
		assertEquals("FooBar", KataFooBarQix.compute("51"));
		assertEquals("BarFoo", KataFooBarQix.compute("53"));
	}

	@Test
	public void computeStep2() {
		assertEquals("1*1", KataFooBarQix.compute("101"));
		assertEquals("FooFoo*Foo", KataFooBarQix.compute("303"));
		assertEquals("FooBarQix*Bar", KataFooBarQix.compute("105"));
		assertEquals("FooQix**", KataFooBarQix.compute("10101"));
		assertEquals("**Foo", KataFooBarQix.compute("1003"));
		assertEquals("1**4", KataFooBarQix.compute("1004"));
	}
}
